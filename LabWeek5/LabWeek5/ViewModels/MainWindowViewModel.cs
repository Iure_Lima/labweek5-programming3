﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using LabWeek5.Models;

namespace LabWeek5.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    public static ObservableCollection<User> Users { get; set;} = [];
    public static ObservableCollection<User> RestoreUsers { get; set;} = [];


    public static void AddNewUser(User user)
    {
        Users.Add(user);
        
        ObserverMessage messageObserver = new();
        Subject subject = new();
        subject.Add(messageObserver);
        subject.Notify($"User {user.Email} has been added.");
      
        RestoreUsers.Add(user);
    }
    
    public static void FilterUser(string email) {
        string emailRemoveSpaces = email.Trim();

        if (email == "") RestoreUsersFilter();

        List<User> UsersFiltered = Users.Where(obj => obj.Email.Contains(emailRemoveSpaces)).ToList();
        
        FilteredUsers(UsersFiltered);
        
    }

    public static void RestoreUsersFilter() {
        Users.Clear();
        foreach (User user in RestoreUsers) {
            Users.Add(user);
        }
    }

    public static void FilteredUsers(List<User> filteredUsers) {
        Users.Clear();
        foreach (User user in filteredUsers) {
            Users.Add(user);
        }
        ObserverMessage messageObserver = new();
        Subject subject = new();
        subject.Add(messageObserver);
        subject.Notify($"Filtering email");
    }

}