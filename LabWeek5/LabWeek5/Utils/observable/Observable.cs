public interface IObservable {
    void UpdateMessage(string message);
}