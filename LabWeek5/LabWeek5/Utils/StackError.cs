using System;
using System.Collections.Generic;

public class StackError
{
    private static readonly Stack<Info> stack = new();

    public static void AddError(string errorMessage)
    {
        var errorInfo = new Info(errorMessage, DateTime.Now);
        stack.Push(errorInfo);
    }

    public static string RemoveError()
    {
        if (stack.Count == 0)
        {
            return "No errors.";
        }
        else
        {
            var errorInfo = stack.Pop();
            return $"[{errorInfo.Timestamp}] Invalid param error: {errorInfo.ErrorMessage}";
        }
    }

    private class Info
    {
        public string ErrorMessage { get; }
        public DateTime Timestamp { get; }

        public Info(string errorMessage, DateTime timestamp)
        {
            ErrorMessage = errorMessage;
            Timestamp = timestamp;
        }
    }
}